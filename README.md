Requirements:
- docker engine 18.06.0+
- docker-compose 1.25.5, build 8a1c60f6

Change in src/Web/Monitoring.Server/appsettings.json:
- add STMP server address and credentials
- JWT key, issuer, audience

Collector service in src/Web/CollectorService is console application that is mocked in order to simulate gathering data from sensors. To make it work, please remove mocked services and add new ones that stores real data into the database. This service can be replaced by custom implementation that stores data to DB.