﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Npgsql;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CollectorService
{
    public class Service : IHostedService, IDisposable
    {
        private bool _disposed = false;
        private Timer _timer;
        private readonly string _connectionString;

        public Service(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("PostgresConnection");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.FromSeconds(10), TimeSpan.FromMinutes(1));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            try
            {
                using (var connection = new NpgsqlConnection(_connectionString))
                {
                    connection.Open();

                    var centralEuTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time");

                    var current = DateTime.UtcNow;

                    var czechDatetime = TimeZoneInfo.ConvertTimeFromUtc(current, centralEuTimeZone);
                    bool isWorkingHour = czechDatetime.Hour >= 8 && czechDatetime.Hour <= 17;

                    StringBuilder sb = new StringBuilder();
                    sb.Append("Insert into \"SensorData\" (\"Time\" , \"Data\", \"SensorId\" ) values ");
                    double value = isWorkingHour ? Utils.GetRandomBetweenPercentage(5000, 2) : Utils.GetRandomBetweenPercentage(200, 2);
                    sb.AppendFormat("(Now(), {0}, 1),", value);

                    value = isWorkingHour ? Utils.GetRandomBetweenPercentage(2000, 10) : Utils.GetRandomBetweenPercentage(150, 3);
                    sb.AppendFormat("(Now(), {0}, 3),", value);

                    var oil = connection.QueryFirst<double>("select \"Data\" from \"SensorData\" where \"SensorId\" = 2 order by \"Time\" desc limit 1");
                    if (isWorkingHour)
                    {
                        if (oil <= 100)
                        {
                            oil = 990;
                        }
                        else
                        {
                            oil -= Utils.GetRandomBetweenPercentage(15d, 50d);
                        }

                        oil = Math.Round(oil, 1);
                        sb.AppendFormat("(Now(), {0}, 2);", oil.ToString(new System.Globalization.CultureInfo("en-US")));
                    }

                    //value = isWorkingHour ? Utils.GetRandomBetweenPercentage(2000, 10) : Utils.GetRandomBetweenPercentage(150, 3);
                    //sb.AppendFormat("(Now(), {0}, 3);", value);

                    Console.WriteLine(sb.ToString());
                    Console.WriteLine(sb.ToString());
                    var res = connection.Execute(sb.ToString());
                    Console.WriteLine(res);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                }

                _disposed = true;
            }
        }

        ~Service()
        {
            Dispose(false);
        }
    }
}
