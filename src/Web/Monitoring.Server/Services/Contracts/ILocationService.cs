﻿using Monitoring.Shared.Dto;

namespace Monitoring.Server.Services.Contracts
{
    public interface ILocationService : ICrudService<LocationDto>
    {
    }
}
