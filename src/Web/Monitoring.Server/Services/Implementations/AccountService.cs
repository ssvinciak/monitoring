using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Monitoring.Server.Data.Core;
using Monitoring.Server.Helpers;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.DataModels;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;
using Monitoring.Storage;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Services.Implementations
{
    public class AccountService : IAccountService
    {
        private readonly ILogger<AccountService> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;

        public AccountService(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext db,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountService> logger,
            RoleManager<IdentityRole<Guid>> roleManager,
            IEmailService emailService,
            IMapper mapper,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _roleManager = roleManager;
            _emailService = emailService;
            _configuration = configuration;
            _db = db;
            _mapper = mapper;
        }

        public async Task<ApiResponse> AdminResetUserPasswordAsync(Guid id, AdminResetPasswordDto adminResetPasswordDto)
        {
            ApplicationUser user;
            try
            {
                user = await _userManager.FindByIdAsync(id.ToString());
                if (user?.Id == null)
                {
                    throw new KeyNotFoundException();
                }
            }
            catch (KeyNotFoundException ex)
            {
                return new ApiResponse(Status400BadRequest, "Unable to find user" + ex.Message);
            }
            try
            {
                var passToken = await _userManager.GeneratePasswordResetTokenAsync(user);
                var result = await _userManager.ResetPasswordAsync(user, passToken, adminResetPasswordDto.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation(user.UserName + "'s password reset");
                    return new ApiResponse(Status204NoContent, user.UserName + " password reset");
                }
                else
                {
                    _logger.LogInformation(user.UserName + "'s password reset failed");

                    if (result.Errors.Any())
                    {
                        string resultErrorsString = "";
                        foreach (var identityError in result.Errors)
                        {
                            resultErrorsString += identityError.Description + ", ";
                        }
                        resultErrorsString.TrimEnd(',');
                        return new ApiResponse(Status400BadRequest, resultErrorsString);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(user.UserName + "'s password reset failed");
                return new ApiResponse(Status400BadRequest, ex.Message);
            }
        }

        public async Task<ApiResponse> ConfirmEmail(ConfirmEmailDto parameters)
        {
            var user = await _userManager.FindByIdAsync(parameters.UserId);
            if (user == null)
            {
                _logger.LogInformation("User does not exist: {0}", parameters.UserId);
                return new ApiResponse(Status404NotFound, "User does not exist");
            }

            if (user.EmailConfirmed)
            {
                return new ApiResponse(Status400BadRequest, "Email is already confirmed");
            }

            var token = parameters.Token;
            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (!result.Succeeded)
            {
                _logger.LogInformation("User email confirmation failed: {0}", string.Join(",", result.Errors.Select(i => i.Description)));
                return new ApiResponse(Status400BadRequest, "User Email Confirmation Failed");
            }

            return new ApiResponse(Status200OK, "Success");
        }

        public async Task<ApiResponse> Create(CreateUserDto parameters)
        {
            try
            {
                var user = new ApplicationUser
                {
                    UserName = parameters.UserName,
                    Email = parameters.Email
                };

                var result = await _userManager.CreateAsync(user, parameters.Password);
                if (!result.Succeeded)
                {
                    return new ApiResponse(Status400BadRequest, "Creation user failed: " + result.Errors.FirstOrDefault()?.Description);
                }
                else
                {
                    _logger.LogInformation($"User with email: {user.Email} was successfully created.");
                    var claimsResult = _userManager.AddClaimsAsync(user, new Claim[]{
                        new Claim(Policies.IsUser,""),
                        // new Claim(JwtClaimTypes.Name, parameters.UserName),
                        // new Claim(JwtClaimTypes.Email, parameters.Email),
                        //new Claim(JwtClaimTypes.EmailVerified, "false", ClaimValueTypes.Boolean)
                    }).Result;
                }

                await _userManager.AddToRoleAsync(user, "User");

                try
                {
                    // For more information on how to enable account confirmation and password reset
                    // please visit http://go.microsoft.com/fwlink/?LinkID=532713 var
                    var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                    string encodedToken = HttpUtility.UrlEncode(token);
                    string callbackUrl = string.Format("{0}/Account/ConfirmEmail/{1}?token={2}",
                        _configuration["Monitoring:ApplicationUrl"], user.Id, encodedToken);

                    var email = new EmailMessageDto();
                    email.ToAddresses.Add(new EmailAddressDto(user.Email, user.Email));
                    email = EmailTemplates.BuildNewUserConfirmationEmail(
                        email, user.UserName, user.Email, callbackUrl, user.Id.ToString(), token);
                    //Replace First UserName with Name if you want to add name to Registration Form

                    await _emailService.SendEmailAsync(email);
                    _logger.LogInformation("Confirmation email sent: {0}", user);
                }
                catch (Exception ex)
                {
                    _logger.LogError("New user confirmation email failed: {0}", ex.Message);
                }

                try
                {
                    var email = new EmailMessageDto();
                    email.ToAddresses.Add(new EmailAddressDto(user.Email, user.Email));
                    var fullname = user.FirstName + " " + user.LastName;
                    email.BuildNewUserEmail(fullname, user.UserName, user.Email, parameters.Password);

                    await _emailService.SendEmailAsync(email);
                    _logger.LogInformation("New user email sent: {0}", user);
                }
                catch (Exception ex)
                {
                    _logger.LogInformation("New user email failed: {0}", ex.Message);
                }

                return new ApiResponse(Status200OK, "Create user success");
            }
            catch (Exception ex)
            {
                _logger.LogError("Create user failed: {0}", ex.Message);
                return new ApiResponse(Status400BadRequest, "Create User Failed");
            }
        }

        public async Task<ApiResponse> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return new ApiResponse(Status404NotFound, "User does not exist");
            }
            try
            {
                _logger.LogInformation($"User {user.Email} was successfully deleted");
                await _userManager.DeleteAsync(user);
                return new ApiResponse(Status200OK, "User Deletion Successful");
            }
            catch (Exception ex)
            {
                _logger.LogError("Deletion user dailed: {0}", ex.Message);
                return new ApiResponse(Status400BadRequest, "User deletion failed");
            }
        }

        public async Task<ApiResponse> ListRoles()
        {
            try
            {
                List<string> roles = await _roleManager.Roles.Select(x => x.Name).ToListAsync();
                return new ApiResponse(Status200OK, "", roles);
            }
            catch (Exception ex)
            {
                _logger.LogError("List roles failed", ex.Message);
                return new ApiResponse(Status400BadRequest, "List roles failed");
            }
        }

        public async Task<ApiResponse> Login(LoginDto parameters)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(parameters.UserName);
                var result = await _signInManager.PasswordSignInAsync(user, parameters.Password, false, lockoutOnFailure: true);

                // If lock out activated and the max. amounts of attempts is reached.
                if (result.IsLockedOut)
                {
                    _logger.LogInformation("User Locked out: {0}", parameters.UserName);
                    return new ApiResponse(Status400BadRequest, "User is locked out!");
                }

                // If your email or phone number is not confirmed but you require it in the settings
                // for login.
                if (result.IsNotAllowed)
                {
                    _logger.LogInformation("User not allowed to log in: {0}", parameters.UserName);
                    return new ApiResponse(Status400BadRequest, "Email is not confirmed!");
                }

                if (result.Succeeded)
                {
                    ClaimsPrincipal principal = await _signInManager.CreateUserPrincipalAsync(user);

                    var claims = await GetUserClaims(user);
                    var expiry = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["JwtExpiryInMinutes"]));
                    string token = GenerateJwtToken(claims);
                    RefreshToken refreshToken = GenerateRefreshToken();

                    user.RefreshTokens.Add(refreshToken);
                    _db.Update(user);
                    await _db.SaveChangesAsync();


                    var loginResult = new LoginResultDto
                    {
                        Token = token,
                        TokenExpiry = expiry,
                        User = await BuildUserInfo(principal),
                        RefreshToken = _mapper.Map<RefreshTokenDto>(refreshToken)
                    };

                    _logger.LogInformation("Logged In: {0}", parameters.UserName);
                    return new ApiResponse(Status200OK, "Login successful", loginResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Login failed for {parameters.UserName}: " + ex.Message);
                return new ApiResponse(Status400BadRequest, "Login failed");
            }

            _logger.LogInformation("Invalid password for user {0}}", parameters.UserName);
            return new ApiResponse(Status400BadRequest, "Username or password is wrong");
        }

        public async Task<ApiResponse> RefreshToken(string token)
        {
            try
            {
                var user = await _db.Users.SingleOrDefaultAsync(u => u.RefreshTokens.Any(rt => rt.Token == token));

                if (user == null)
                {
                    return new ApiResponse(Status404NotFound, "User with token not found");
                }

                var refreshToken = user.RefreshTokens.Single(rt => rt.Token == token);

                if (!refreshToken.IsActive)
                {
                    return new ApiResponse(Status400BadRequest, "Token is not active");
                }

                ClaimsPrincipal principal = await _signInManager.CreateUserPrincipalAsync(user);
                var claims = await GetUserClaims(user);
                var jwtToken = GenerateJwtToken(claims);

                var expiry = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["JwtExpiryInMinutes"]));
                refreshToken.Revoked = DateTime.UtcNow;
                RefreshToken newRefreshToken = GenerateRefreshToken();
                user.RefreshTokens.Add(newRefreshToken);
                _db.Update(user);
                await _db.SaveChangesAsync();

                var result = new LoginResultDto
                {
                    Token = jwtToken,
                    TokenExpiry = expiry,
                    User = await BuildUserInfo(principal),
                    RefreshToken = _mapper.Map<RefreshTokenDto>(newRefreshToken)
                };

                return new ApiResponse(Status200OK, "Refresh token", result);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Refresh token failed for {token}: " + ex.Message);
                return new ApiResponse(Status400BadRequest, "Refresh token has failed");
            }
        }

        public async Task<ApiResponse> RevokeToken(string token)
        {
            try
            {
                var user = await _db.Users.SingleOrDefaultAsync(u => u.RefreshTokens.Any(rt => rt.Token == token));

                if (user == null)
                {
                    return new ApiResponse(Status404NotFound, "User with token not found");
                }

                var refreshToken = user.RefreshTokens.Single(rt => rt.Token == token);

                if (!refreshToken.IsActive)
                {
                    return new ApiResponse(Status400BadRequest, "Token is not active");
                }

                refreshToken.Revoked = DateTime.UtcNow;
                _db.Update(user);
                await _db.SaveChangesAsync();

                return new ApiResponse(Status200OK, "Token revoked");
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Refresh token failed for {token}: " + ex.Message);
                return new ApiResponse(Status400BadRequest, "Refresh token has failed");
            }
        }

        public async Task<ApiResponse> Logout()
        {
            await _signInManager.SignOutAsync();
            return new ApiResponse(Status200OK, "Logout successful");
        }

        public async Task<ApiResponse> UpdateUser(ClaimsPrincipal user, UserInfoDto userInfo)
        {
            var appUser = await _userManager.FindByIdAsync(userInfo.UserId.ToString()).ConfigureAwait(true);

            appUser.FirstName = userInfo.FirstName;
            appUser.LastName = userInfo.LastName;

            try
            {
                var result = await _userManager.UpdateAsync(appUser).ConfigureAwait(true);
            }
            catch
            {
                return new ApiResponse(Status400BadRequest, "Error Updating User");
            }

            if (user.Claims.FirstOrDefault(c => c.Type == "IsAdministrator") != null && userInfo.Roles != null)
            {
                try
                {
                    var rolesToAdd = new List<string>();
                    var currentUserRoles = (List<string>)(await _userManager.GetRolesAsync(appUser).ConfigureAwait(true));
                    foreach (var newUserRole in userInfo.Roles)
                    {
                        if (!currentUserRoles.Contains(newUserRole))
                        {
                            rolesToAdd.Add(newUserRole);
                        }
                    }
                    await _userManager.AddToRolesAsync(appUser, rolesToAdd).ConfigureAwait(true);
                    //HACK to switch to claims auth
                    foreach (var role in rolesToAdd)
                    {
                        await _userManager.AddClaimAsync(appUser, new Claim($"Is{role}", "true")).ConfigureAwait(true);
                    }

                    var rolesToRemove = currentUserRoles
                        .Where(role => !userInfo.Roles.Contains(role)).ToList();

                    await _userManager.RemoveFromRolesAsync(appUser, rolesToRemove).ConfigureAwait(true);

                    //HACK to switch to claims auth
                    foreach (var role in rolesToRemove)
                    {
                        await _userManager.RemoveClaimAsync(appUser, new Claim($"Is{role}", "true")).ConfigureAwait(true);
                    }
                }
                catch
                {
                    return new ApiResponse(Status500InternalServerError, "Error Updating Roles");
                }
            }

            return new ApiResponse(Status200OK, "User updated");
        }

        public async Task<ApiResponse> UserInfo(ClaimsPrincipal user)
        {
            UserInfoDto userInfo = await BuildUserInfo(user);
            return new ApiResponse(Status200OK, "Retrieved user info", userInfo);
        }

        private async Task<UserInfoDto> BuildUserInfo(ClaimsPrincipal principal)
        {
            var user = await _userManager.GetUserAsync(principal);
            if (user != null)
            {
                try
                {
                    return new UserInfoDto
                    {
                        IsAuthenticated = principal.Identity.IsAuthenticated,
                        UserName = user.UserName,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        UserId = user.Id,
                        PhoneNumber = user.PhoneNumber,
                        ExposedClaims = principal.Claims.Select(c => new KeyValuePair<string, string>(c.Type, c.Value)).ToList(),
                        Roles = await _userManager.GetRolesAsync(user)
                    };
                }
                catch (Exception ex)
                {
                    _logger.LogWarning("Could not build UserInfoDto: " + ex.Message);
                }
            }
            else
            {
                return new UserInfoDto();
            }

            return null;
        }

        public async Task<ApiResponse> UserChangePassword(ChangePasswordDto parameters)
        {
            var user = await _userManager.FindByIdAsync(parameters.UserId.ToString());
            if (user == null)
            {
                _logger.LogInformation("User was not found with id: {0}", parameters.UserId);
                return new ApiResponse(Status404NotFound, "User was not found");
            }

            IdentityResult result = await _userManager.ChangePasswordAsync(
                user,
                parameters.OldPassword,
                parameters.Password
            );

            if (!result.Succeeded)
            {
                IdentityError error = result.Errors.FirstOrDefault();
                return new ApiResponse(Status400BadRequest, $"User's password was not changed", error);
            }
            else
            {
                return new ApiResponse(Status200OK, $"User's password was sucessfully changed");
            }
        }

        private async Task<IEnumerable<Claim>> GetUserClaims(ApplicationUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            var userRoles = roles.Select(r => new Claim(ClaimTypes.Role, r)).ToArray();
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roleClaims = await _db.RoleClaims
                .Join(_db.Roles, rc => rc.RoleId, r => r.Id, (rc, r) => new
                {
                    ClaimValue = rc.ClaimValue,
                    r.Name
                })
                .Where(rc => roles.Contains(rc.Name))
                .Select(rc => new Claim(ClaimConstants.Permission, rc.ClaimValue))
                .ToListAsync();
            var claims = new List<Claim> {
                        new Claim(ClaimTypes.Name, user.UserName),
                        new Claim(ClaimTypes.Email, user.Email),
                        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                    };
            claims.AddRange(userClaims);
            claims.AddRange(userRoles);
            claims.AddRange(roleClaims);

            return claims;
        }

        private string GenerateJwtToken(IEnumerable<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSecurityKey"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiry = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["JwtExpiryInMinutes"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtAudience"],
                claims,
                expires: expiry,
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private RefreshToken GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomNumber),
                    Created = DateTime.UtcNow,
                    Expiry = DateTime.Now.AddDays(Convert.ToInt32(_configuration["RefreshTokenExpiryInDays"]))
                };
            }
        }
    }
}
