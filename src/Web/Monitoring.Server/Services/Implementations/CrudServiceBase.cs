using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.DataInterfaces;
using Monitoring.Shared.Dto;
using Monitoring.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Services.Implementations
{
    public abstract class CrudServiceBase<TEntity, TDto> : ICrudService<TDto>
        where TEntity : class, IEntity<int>, new()
        where TDto : BaseDto
    {
        private readonly DbSet<TEntity> _dbSet;
        protected readonly IMapper Mapper;
        protected readonly ApplicationDbContext Db;

        protected CrudServiceBase(ApplicationDbContext db, IMapper mapper)
        {
            _dbSet = db.Set<TEntity>();
            Db = db;
            Mapper = mapper;
        }

        public async virtual Task<object> GetOData(IQueryCollection query)
        {
            var count = _dbSet.Count();
            int skip = (query.TryGetValue("$skip", out var Skip)) ? Convert.ToInt32(Skip[0]) : 0;
            int top = (query.TryGetValue("$top", out var Take)) ? Convert.ToInt32(Take[0]) : count;

            var queryable = _dbSet
                                .Skip(skip)
                                .Take(top);

            var items = await Mapper.ProjectTo<TDto>(queryable).ToArrayAsync();

            return new
            {
                Items = items,
                Count = count
            };
        }

        public async virtual Task<ApiResponse> GetAllAsync()
        {
            try
            {
                var entities = await Mapper.ProjectTo<TDto>(_dbSet).ToArrayAsync();

                return new ApiResponse(Status200OK, "Retrieved entities", entities);
            }
            catch (Exception ex)
            {
                return new ApiResponse(Status400BadRequest, ex.Message);
            }
        }

        public async Task<ApiResponse> GetAsync(int id)
        {
            TEntity entity = await _dbSet.FirstOrDefaultAsync(entity => entity.Id == id);
            if (entity == null)
            {
                return new ApiResponse(Status404NotFound, "Entity not found.");
            }

            var entityDto = Mapper.Map<TDto>(entity);
            return new ApiResponse(Status200OK, "Retrieved entity", entityDto);
        }

        public async Task<ApiResponse> Create(TDto entityDto)
        {
            TEntity entity = await _dbSet.FirstOrDefaultAsync(entity => entity.Id == entityDto.Id);
            if (entity != null)
            {
                return new ApiResponse(Status400BadRequest, "Entity already exists");
            }

            entity = Mapper.Map<TDto, TEntity>(entityDto);
            Console.WriteLine(entity);
            entity.Id = 0;
            await _dbSet.AddAsync(entity);
            await Db.SaveChangesAsync();

            return new ApiResponse(Status200OK, "Created entity", entity);
        }

        public async Task<ApiResponse> Update(TDto entityDto)
        {
            TEntity entity = await _dbSet.FirstOrDefaultAsync(entity => entity.Id == entityDto.Id);
            if (entity == null)
            {
                return new ApiResponse(Status404NotFound, "Entity not found");
            }

            Mapper.Map(entityDto, entity);
            await Db.SaveChangesAsync();
            return new ApiResponse(Status200OK, "Updated entity", entity);
        }

        public async Task<ApiResponse> Delete(int id)
        {
            TEntity entity = await _dbSet.FirstOrDefaultAsync(entity => entity.Id == id);
            if (entity == null)
            {
                return new ApiResponse(Status404NotFound, "Entity not found");
            }

            _dbSet.Remove(entity);
            await Db.SaveChangesAsync();
            return new ApiResponse(Status200OK, "Deleted entity");
        }
    }
}
