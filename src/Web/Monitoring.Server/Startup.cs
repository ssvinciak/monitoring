using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Monitoring.Server.Authorization;
using Monitoring.Server.Helpers;
using Monitoring.Server.Middleware;
using Monitoring.Server.Models;
using Monitoring.Server.Services.Contracts;
using Monitoring.Server.Services.Implementations;
using Monitoring.Server.Utils;
using Monitoring.Shared;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.DataInterfaces;
using Monitoring.Shared.DataModels;
using Monitoring.Storage;
using Monitoring.Storage.Mapping;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace Monitoring.Server
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var migrationsAssembly = typeof(ApplicationDbContext).GetTypeInfo().Assembly.GetName();
            var migrationsAssemblyName = migrationsAssembly.Name;
            services.AddDbContext<ApplicationDbContext>(
                builder => builder
                    .UseLazyLoadingProxies()
                    .UseNpgsql(
                        Configuration.GetConnectionString("PostgresConnection"),
                        sql => sql.MigrationsAssembly(migrationsAssemblyName)
                ));

            services.AddIdentity<ApplicationUser, IdentityRole<Guid>>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtSecurityKey"]))
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Policies.IsAdmin, Policies.IsAdminPolicy());
                options.AddPolicy(Policies.IsManager, Policies.IsManagerPolicy());
                options.AddPolicy(Policies.IsUser, Policies.IsUserPolicy());
            });

            services.AddSingleton<IAuthorizationPolicyProvider, AuthorizationPolicyProvider>();
            services.AddTransient<IAuthorizationHandler, PermissionRequirementHandler>();

            services.AddMvc().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                //options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                options.User.RequireUniqueEmail = true;

                options.SignIn.RequireConfirmedEmail = true;
            });

            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromDays(3);
            });

            services.AddScoped<IUserSession, UserSession>();

            services.AddSingleton<IEmailConfiguration>(Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());

            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<ISensorTypeService, SensorTypeService>();
            services.AddTransient<ISensorService, SensorService>();
            services.AddTransient<IThresholdService, ThresholdService>();
            services.AddTransient<ISensorModelService, SensorModelService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IUnitService, UnitService>();

            var automapperConfig = new MapperConfiguration(configuration =>
            {
                configuration.AddProfile(new MappingProfile());
            });
            var autoMapper = automapperConfig.CreateMapper();

            services.AddSingleton(autoMapper);

            services.AddTransient<IDatabaseInitializer, DatabaseInitializer>();

            services.AddHostedService<ThresholdCheckService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var databaseInitializer = serviceScope.ServiceProvider.GetService<IDatabaseInitializer>();
                databaseInitializer.SeedAsync().Wait();
            }

            app.UseMiddleware<ApiResponseMiddleware>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }

            EmailTemplates.Initialize(env);

            app.UseStaticFiles();
            app.UseBlazorFrameworkFiles();

            if (string.IsNullOrWhiteSpace(env.WebRootPath))
            {
                var p = Path.Combine(Directory.GetCurrentDirectory(), "..", "Monitoring.Client", "wwwroot");
                env.WebRootPath = p;
            }

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<UserSessionMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}
