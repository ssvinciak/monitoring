﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Server.Middleware.Wrappers;
using Monitoring.Server.Services.Contracts;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.Dto;
using Monitoring.Storage;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SensorModelController : ControllerBase
    {
        private readonly ApplicationDbContext db;
        private readonly ISensorModelService _sensorModelService;

        public SensorModelController(ApplicationDbContext db, ISensorModelService sensorModelService)
        {
            this.db = db;
            _sensorModelService = sensorModelService;
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> Get(int id)
        {
            if (id <= 0)
            {
                return new ApiResponse(Status400BadRequest, "Sensor type model is invalid.");
            }
            return await _sensorModelService.GetAsync(id);
        }

        [HttpGet]
        public async Task<object> Get()
        {
            IQueryCollection queryString = Request.Query;
            if (queryString.Keys.Contains("$inlinecount"))
            {
                return await _sensorModelService.GetOData(queryString);
            }
            else
            {
                return await _sensorModelService.GetAllAsync();
            }
        }

        [HttpPost]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Post(SensorModelDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Sensor type model is invalid.");
            }

            return await _sensorModelService.Create(parameters);
        }

        [HttpPut]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Put(SensorModelDto parameters)
        {
            if (!ModelState.IsValid)
            {
                return new ApiResponse(Status400BadRequest, "Sensor type model is invalid.");
            }

            return await _sensorModelService.Update(parameters);
        }

        [HttpDelete]
        [Authorize(Policy = Policies.IsAdmin)]
        public async Task<ApiResponse> Delete(int id)
        {
            return await _sensorModelService.Delete(id);
        }
    }
}
