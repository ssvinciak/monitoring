﻿using System.ComponentModel.DataAnnotations;

namespace Monitoring.Shared.Dto
{
    public class LocationDto : BaseDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string DisplayName { get; set; }
        [Phone]
        public string PhoneContact { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string Note { get; set; }
    }
}
