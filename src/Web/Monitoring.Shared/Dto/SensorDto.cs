﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Monitoring.Shared.Dto
{
    public class SensorDto : BaseDto
    {
        public string DisplayName { get; set; }
        [Required]
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string Note { get; set; }
        public DateTime? InstallationDate { get; set; }
        public DateTime? RevisionDate { get; set; }

        public SensorModelDto SensorModel { get; set; }
        public LocationDto Location { get; set; }
    }
}
