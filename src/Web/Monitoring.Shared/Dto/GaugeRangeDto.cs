namespace Monitoring.Shared.Dto
{
    public class GaugeRangeDto
    {
        public int Start { get; set; }
        public int End { get; set; }
        public string Color { get; set; }
    }
}
