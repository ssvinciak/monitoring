﻿using System;

namespace Monitoring.Shared.Dto.Account
{
    public class UserProfileDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string LastPageVisited { get; set; } = "/";
    }
}
