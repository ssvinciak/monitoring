﻿using Newtonsoft.Json;
using System;

namespace Monitoring.Shared.Dto.Account
{
    public class LoginResultDto
    {
        public string Token { get; set; }
        public DateTime TokenExpiry { get; set; }

        [JsonIgnore]
        public RefreshTokenDto RefreshToken { get; set; }

        public UserInfoDto User { get; set; }
    }
}
