﻿using System;

namespace Monitoring.Shared.Dto.Account
{
    public class RefreshTokenDto
    {
        public DateTime Expiry { get; set; }
        public bool IsExpired => DateTime.UtcNow >= Expiry;
        public string Token { get; set; }
    }
}
