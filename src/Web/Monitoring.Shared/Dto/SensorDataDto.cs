﻿using System;

namespace Monitoring.Shared.Dto
{
    public class SensorDataDto
    {
        public DateTime Time { get; set; }
        public double Data { get; set; }
    }
}
