﻿namespace Monitoring.Shared.Dto
{
    public class SensorTypeDto : BaseDto
    {
        public string Name { get; set; }
    }
}
