﻿using System.Collections.Generic;
using Monitoring.Shared.DataInterfaces;

namespace Monitoring.Shared.DataModels
{
    public class SensorType : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual IList<SensorModel> SensorModels { get; set; }
    }
}
