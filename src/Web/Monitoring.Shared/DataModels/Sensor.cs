﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Monitoring.Shared.DataInterfaces;

namespace Monitoring.Shared.DataModels
{
    public class Sensor : IEntity<int>
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        [Required]
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string Note { get; set; }
        public DateTime? InstallationDate { get; set; }
        public DateTime? RevisionDate { get; set; }

        public virtual SensorModel SensorModel { get; set; }
        public virtual Location Location { get; set; }
        public virtual List<SensorData> SensorData { get; set; }
        public virtual List<Threshold> Thresholds { get; set; }
    }
}
