﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Monitoring.Shared.DataModels
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual UserProfile Profile { get; set; }
        public virtual List<RefreshToken> RefreshTokens { get; set; }
    }
}
