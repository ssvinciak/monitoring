﻿using Monitoring.Shared.DataModels;
using Monitoring.Shared.DataInterfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Monitoring.Storage
{
    public interface IApplicationDbContext
    {
        public DbSet<Location> Locations { get; set; }
        public DbSet<Sensor> Sensors { get; set; }
        public DbSet<SensorData> SensorData { get; set; }
        public DbSet<SensorType> SensorTypes { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Dashboard> Dashboards { get; set; }

        public void SetGlobalQueryForSoftDelete<T>(ModelBuilder builder) where T : class, ISoftDelete;

        public void SetGlobalQueryForSoftDeleteAndTenant<T>(ModelBuilder builder) where T : class, ISoftDelete;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        int SaveChanges();

    }
}
