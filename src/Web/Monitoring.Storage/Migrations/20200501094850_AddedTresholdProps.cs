﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Monitoring.Storage.Migrations
{
    public partial class AddedTresholdProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Thresholds_AspNetUsers_UserId",
                table: "Thresholds");

            migrationBuilder.DropIndex(
                name: "IX_Thresholds_UserId",
                table: "Thresholds");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Thresholds");

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "Thresholds",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Notify",
                table: "Thresholds",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Thresholds_CreatedById",
                table: "Thresholds",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Thresholds_AspNetUsers_CreatedById",
                table: "Thresholds",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Thresholds_AspNetUsers_CreatedById",
                table: "Thresholds");

            migrationBuilder.DropIndex(
                name: "IX_Thresholds_CreatedById",
                table: "Thresholds");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "Thresholds");

            migrationBuilder.DropColumn(
                name: "Notify",
                table: "Thresholds");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Thresholds",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Thresholds_UserId",
                table: "Thresholds",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Thresholds_AspNetUsers_UserId",
                table: "Thresholds",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
