﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Monitoring.Storage.Migrations
{
    public partial class Change14 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SensorTypes_Units_UnitId",
                table: "SensorTypes");

            migrationBuilder.DropIndex(
                name: "IX_SensorTypes_UnitId",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "UnitId",
                table: "SensorTypes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UnitId",
                table: "SensorTypes",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SensorTypes_UnitId",
                table: "SensorTypes",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_SensorTypes_Units_UnitId",
                table: "SensorTypes",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
