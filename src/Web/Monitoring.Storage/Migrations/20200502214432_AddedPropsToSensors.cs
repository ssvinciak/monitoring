﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Monitoring.Storage.Migrations
{
    public partial class AddedPropsToSensors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Brand",
                table: "SensorTypes");

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "SensorTypes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Manufacturer",
                table: "SensorTypes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "SensorTypes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PowerSupplyVoltage",
                table: "SensorTypes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "Sensors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "Sensors",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InstallationDate",
                table: "Sensors",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Sensors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "Sensors",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RevisionDate",
                table: "Sensors",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "Manufacturer",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "PowerSupplyVoltage",
                table: "SensorTypes");

            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "Sensors");

            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "Sensors");

            migrationBuilder.DropColumn(
                name: "InstallationDate",
                table: "Sensors");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Sensors");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "Sensors");

            migrationBuilder.DropColumn(
                name: "RevisionDate",
                table: "Sensors");

            migrationBuilder.AddColumn<string>(
                name: "Brand",
                table: "SensorTypes",
                type: "text",
                nullable: true);
        }
    }
}
