﻿using AutoMapper.Configuration;
using Monitoring.Shared.DataModels;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;

namespace Monitoring.Storage.Mapping
{
    public class MappingProfile : MapperConfigurationExpression
    {
        public MappingProfile()
        {
            CreateMap<Location, LocationDto>().ReverseMap();
            CreateMap<SensorData, SensorDataDto>().ReverseMap();
            CreateMap<SensorType, SensorTypeDto>().ReverseMap();
            CreateMap<Unit, UnitDto>().ReverseMap();
            CreateMap<SensorModel, SensorModelDto>().ReverseMap();
            CreateMap<Sensor, SensorDto>().ReverseMap();
            CreateMap<Dashboard, DashboardDto>().ReverseMap();
            CreateMap<Panel, PanelDto>().ReverseMap();
            CreateMap<Threshold, ThresholdDto>().ReverseMap();
            CreateMap<Threshold, ThresholdCreateDto>().ReverseMap();
            CreateMap<ApplicationUser, UserDto>().ReverseMap();
            CreateMap<RefreshToken, RefreshTokenDto>().ReverseMap();
        }
    }
}
