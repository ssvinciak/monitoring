﻿using System;
using System.Threading.Tasks;
using Monitoring.Server.Utils;
using Monitoring.Shared.DataModels;
using Xunit;

namespace Monitoring.Server.Tests
{
    public class ThresholdCheckServiceTests : TestBase
    {
        [Fact]
        public void CheckThreshold_ExceededUpperLimit_ShouldBeReached()
        {
            var threshold = new Threshold
            {
                Id = 1,
                MaxValue = 5,
                MinValue = 0,
            };

            var data = GetRandomData(count: 10, from: 1, to: 4.5f);
            const int limitValue = 6;
            data[3].Data = limitValue;

            var result = ThresholdCheckService.HasThresholdReached(threshold, data);

            Assert.True(result.HasReached);
            Assert.Equal(limitValue, result.value);
        }

        [Fact]
        public void CheckThreshold_ExceededLowerLimit_ShouldBeReached()
        {
            var threshold = new Threshold
            {
                Id = 1,
                MaxValue = 5,
                MinValue = 0,
            };

            var data = GetRandomData(count: 10, from: 1, to: 4.5f);
            const int limitValue = -1;
            data[3].Data = limitValue;

            var result = ThresholdCheckService.HasThresholdReached(threshold, data);

            Assert.True(result.HasReached);
            Assert.Equal(limitValue, result.value);
        }
    }
}
