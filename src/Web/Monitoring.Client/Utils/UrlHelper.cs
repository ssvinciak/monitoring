﻿using System;

namespace Monitoring.Client.Utils
{
    public static class UrlHelper
    {
        public static string GetUrlDateTime(DateTime dateTime)
        {
            var culture = new System.Globalization.CultureInfo("en-US");
            return dateTime.ToString("MM/dd/yyyy hh:mm tt", culture);
        }
    }
}
