using System.Resources;
using Syncfusion.Blazor;

namespace Monitoring.Client.Shared
{
    public class SyncfusionLocalizer : ISyncfusionStringLocalizer
    {
        public ResourceManager ResourceManager
        {
            get
            {
                return Resources.SfResources.ResourceManager;
            }
        }

        public string GetText(string key)
        {
            return ResourceManager.GetString(key);
        }
    }

}