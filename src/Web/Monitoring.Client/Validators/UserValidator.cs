﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;

namespace Monitoring.Client.Validators
{
    public class UserValidator : AbstractValidator<CreateUserDto>
    {
        public UserValidator(IStringLocalizer<App> localizer)
        {
            RuleFor(t => t.Email).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.UserName).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.Password).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.PasswordConfirm).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
        }
    }
}
