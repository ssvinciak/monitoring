﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Monitoring.Shared.Dto;

namespace Monitoring.Client.Validators
{
    public class UnitValidator : AbstractValidator<UnitDto>
    {
        public UnitValidator(IStringLocalizer<App> localizer)
        {
            RuleFor(t => t.Name).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
            RuleFor(t => t.Symbol).NotEmpty().WithMessage(_ => localizer["Validation.NotEmpty"]);
        }
    }
}
