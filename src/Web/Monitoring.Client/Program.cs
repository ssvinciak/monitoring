﻿using Blazored.LocalStorage;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Monitoring.Client.Authorization;
using Monitoring.Client.Services.Contracts;
using Monitoring.Client.Services.Implementations;
using Monitoring.Client.Shared;
using Monitoring.Client.Validators;
using Monitoring.Shared.AuthorizationDefinitions;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;
using Syncfusion.Blazor;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Monitoring.Client
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.RootComponents.Add<App>("app");

            builder.Services.AddBlazoredLocalStorage();
            builder.Services.AddAuthorizationCore(config =>
            {
                config.AddPolicy(Policies.IsAdmin, Policies.IsAdminPolicy());
                config.AddPolicy(Policies.IsUser, Policies.IsUserPolicy());
                config.AddPolicy(Policies.IsManager, Policies.IsManagerPolicy());
            });
            builder.Services.AddTransient<IAuthorizationHandler, PermissionRequirementHandler>();
            builder.Services.AddSingleton<IAuthorizationPolicyProvider, AuthorizationPolicyProvider>();

            builder.Services.AddSingleton(new HttpClient
            {
                BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)
            });
            builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");

            builder.Services.AddScoped<AuthenticationStateProvider, ApiAuthenticationStateProvider>();
            builder.Services.AddScoped<IAuthorizeApi, AuthorizeApi>();
            builder.Services.AddScoped<IValidator<ThresholdDto>, ThresholdValidator>();
            builder.Services.AddScoped<IValidator<ThresholdCreateDto>, ThresholdCreateDtoValidator>();
            builder.Services.AddScoped<IValidator<UnitDto>, UnitValidator>();
            builder.Services.AddScoped<IValidator<RoleDto>, RoleValidator>();
            builder.Services.AddScoped<IValidator<LocationDto>, LocationValidator>();
            builder.Services.AddScoped<IValidator<CreateUserDto>, UserValidator>();
            builder.Services.AddScoped<IValidator<ChangePasswordDto>, ChangePasswordValidator>();

            builder.Services.AddScoped<AppState>();

            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(
                "Mjg1MjE5QDMxMzgyZTMyMmUzMEFnOEJoWkgrMGRiVmtCdVYwUlYvelZkZXJiMk9CMmlPOCtBK2VESkFPSE09"
            );
            builder.Services.AddSyncfusionBlazor();

            builder.Services.AddSingleton<ISuperService, SuperService>();
            builder.Services.AddSingleton<ISyncfusionStringLocalizer, SyncfusionLocalizer>();

            WebAssemblyHost host = builder.Build();

            var localStorage = host.Services.GetService<ILocalStorageService>();

            var cultureName = await localStorage.GetItemAsync<string>("culture");
            if (!IsCultureSupported(cultureName))
            {
                cultureName = "en-US";
            }

            var culture = new CultureInfo(cultureName);
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;

            await host.RunAsync();
        }

        private static bool IsCultureSupported(string cultureName)
        {
            return !string.IsNullOrWhiteSpace(cultureName) 
                && CultureInfo.GetCultures(CultureTypes.AllCultures)
                        .Any(culture => string.Equals(culture.Name, cultureName, StringComparison.CurrentCultureIgnoreCase))
                && Constants.SupportedCultures.Contains(new CultureInfo(cultureName));
        }
    }
}
