﻿using System.Globalization;

namespace Monitoring.Client
{
    public static class Constants
    {
        public static readonly CultureInfo[] SupportedCultures = new CultureInfo[]
        {
            new CultureInfo("en-Us"),
            new CultureInfo("cs-CZ")
        };
    }
}
