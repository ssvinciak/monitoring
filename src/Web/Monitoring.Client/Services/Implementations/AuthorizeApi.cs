using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Monitoring.Client.Services.Implementations
{
    public class AuthorizeApi : IAuthorizeApi
    {
        private readonly HttpClient _httpClient;
        private readonly AuthenticationStateProvider _authenticationStateProvider;
        private readonly ILocalStorageService _localStorage;
        private readonly IJSRuntime _jSRuntime;

        public AuthorizeApi(HttpClient httpClient,
                           AuthenticationStateProvider authenticationStateProvider,
                           ILocalStorageService localStorage,
                           IJSRuntime jSRuntime)
        {
            _httpClient = httpClient;
            _authenticationStateProvider = authenticationStateProvider;
            _localStorage = localStorage;
            _jSRuntime = jSRuntime;
        }

        public async Task<ApiResponseDto> Create(CreateUserDto createUserDto)
        {
            var response = await _httpClient.PostAsJsonAsync("api/Account/Create", createUserDto);
            return await response.Content.ReadFromJsonAsync<ApiResponseDto>();
        }

        public async Task<UserInfoDto> GetUserInfo()
        {
            var userInfo = new UserInfoDto { IsAuthenticated = false, Roles = new List<string>() };
            var apiResponse = await GetFromJsonAsyncc("api/Account/UserInfo");

            if (apiResponse.StatusCode == Status200OK)
            {
                userInfo = JsonConvert.DeserializeObject<UserInfoDto>(apiResponse.Result.ToString());
                return userInfo;
            }
            return userInfo;
        }

        public async Task<ApiResponseDto> Login(LoginDto loginDto)
        {
            using var stringContent = new StringContent(JsonConvert.SerializeObject(loginDto), Encoding.UTF8, "application/json");
            using HttpResponseMessage response = await _httpClient.PostAsync("api/Account/Login", stringContent);

            //response.EnsureSuccessStatusCode();
            string content = await response.Content.ReadAsStringAsync();
            var apiResponseDto = JsonConvert.DeserializeObject<ApiResponseDto>(content);

            if (apiResponseDto.StatusCode == Status200OK)
            {
                var loginResult = JsonConvert.DeserializeObject<LoginResultDto>(apiResponseDto.Result.ToString());
                await _localStorage.SetItemAsync("authToken", loginResult.Token);
                await _localStorage.SetItemAsync("authTokenExpiry", loginResult.TokenExpiry);
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", loginResult.Token);
                ((ApiAuthenticationStateProvider)_authenticationStateProvider).MarkUserAsAuthenticated(loginDto.UserName);
            }

            return apiResponseDto;
        }

        public async Task<ApiResponseDto> RefreshToken()
        {
            using HttpResponseMessage response = await _httpClient.PostAsync("api/Account/refreshToken", null);

            //response.EnsureSuccessStatusCode();
            string content = await response.Content.ReadAsStringAsync();
            var apiResponseDto = JsonConvert.DeserializeObject<ApiResponseDto>(content);

            if (apiResponseDto.StatusCode == Status200OK)
            {
                var loginResult = JsonConvert.DeserializeObject<LoginResultDto>(apiResponseDto.Result.ToString());
                await _localStorage.SetItemAsync("authToken", loginResult.Token);
                await _localStorage.SetItemAsync("authTokenExpiry", loginResult.TokenExpiry);
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", loginResult.Token);
            } else
            {
                await _localStorage.RemoveItemAsync("authToken");
                await _localStorage.RemoveItemAsync("authTokenExpiry");
                ((ApiAuthenticationStateProvider)_authenticationStateProvider).MarkUserAsLoggedOut();
                _httpClient.DefaultRequestHeaders.Authorization = null;
            }

            return apiResponseDto;
        }

        public async Task<ApiResponseDto> Logout()
        {
            using HttpResponseMessage response = await _httpClient.PostAsync("api/Account/logout", null);
            string content = await response.Content.ReadAsStringAsync();
            var apiResponseDto = JsonConvert.DeserializeObject<ApiResponseDto>(content);
            if (apiResponseDto.StatusCode == Status200OK)
            {
                await _localStorage.RemoveItemAsync("authToken");
                await _localStorage.RemoveItemAsync("authTokenExpiry");
                ((ApiAuthenticationStateProvider)_authenticationStateProvider).MarkUserAsLoggedOut();
                _httpClient.DefaultRequestHeaders.Authorization = null;
            }
            return apiResponseDto;
        }

        public async Task<ApiResponseDto> UpdateUser(UserInfoDto userInfoDto)
        {
            var response = await _httpClient.PutAsJsonAsync(
                "api/Account",
                userInfoDto
            );
            return await response.Content.ReadFromJsonAsync<ApiResponseDto>();
        }

        public async Task<ApiResponseDto> ConfirmEmail(ConfirmEmailDto confirmEmailDto)
        {
            var response = await _httpClient.PostAsJsonAsync("api/Account/ConfirmEmail", confirmEmailDto);
            return await response.Content.ReadFromJsonAsync<ApiResponseDto>();
        }

        public async Task<ApiResponseDto> GetFromJsonAsyncc(string uri)
        {
            try
            {
                var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponseDto>(uri);

                if (apiResponse.StatusCode == Status401Unauthorized)
                {
                    var response = await _httpClient.PostAsync("api/account/refreshToken", null);
                    apiResponse = await response.Content.ReadFromJsonAsync<ApiResponseDto>();
                    if (apiResponse.StatusCode == Status200OK)
                    {
                        await _httpClient.GetFromJsonAsync<ApiResponseDto>(uri);
                    }
                }

                return apiResponse;
            }
            catch (HttpRequestException e)
            {
                    System.Console.WriteLine("refreshing token");
                if (e.Message.Contains(Status401Unauthorized.ToString()))
                {
                    System.Console.WriteLine("refreshing token");
                    var apiResponse = await RefreshToken();
                    if (apiResponse.StatusCode == Status200OK)
                    {
                        return await RetryGet(uri);
                    }
                }
                var ex = e.InnerException as WebException;
                throw;
            }
        }

        public async Task<ApiResponseDto> PostAsJsonAsyncc<T>(string uri, T body)
        {
            try
            {
                var response = await _httpClient.PostAsJsonAsync(uri, body);
                var apiResponse = await response.Content.ReadFromJsonAsync<ApiResponseDto>();

                if (apiResponse.StatusCode == Status401Unauthorized)
                {
                    response = await _httpClient.PostAsync("api/account/refreshToken", null);
                    apiResponse = await response.Content.ReadFromJsonAsync<ApiResponseDto>();
                    if (apiResponse.StatusCode >= Status200OK && apiResponse.StatusCode <= 299)
                    {
                        await _httpClient.GetFromJsonAsync<ApiResponseDto>(uri);
                    }
                }

                return apiResponse;
            }
            catch (HttpRequestException e)
            {
                if (e.Message.Contains(Status401Unauthorized.ToString()))
                {
                    System.Console.WriteLine("refreshing token");
                    var apiResponse = await RefreshToken();
                    if (apiResponse.StatusCode == Status200OK)
                    {
                        return await RetryPost(uri, body);
                    }
                }
                var ex = e.InnerException as WebException;
                throw;
            }
        }

        public async Task<ApiResponseDto> PutAsJsonAsyncc<T>(string uri, T body)
        {
               try
            {
                var response = await _httpClient.PutAsJsonAsync(uri, body);
                var apiResponse = await response.Content.ReadFromJsonAsync<ApiResponseDto>();

                if (apiResponse.StatusCode == Status401Unauthorized)
                {
                    response = await _httpClient.PostAsync("api/account/refreshToken", null);
                    apiResponse = await response.Content.ReadFromJsonAsync<ApiResponseDto>();
                    if (apiResponse.StatusCode >= Status200OK && apiResponse.StatusCode <= 299)
                    {
                        await _httpClient.GetFromJsonAsync<ApiResponseDto>(uri);
                    }
                }

                return apiResponse;
            }
            catch (HttpRequestException e)
            {
                if (e.Message.Contains(Status401Unauthorized.ToString()))
                {
                    System.Console.WriteLine("refreshing token");
                    var apiResponse = await RefreshToken();
                    if (apiResponse.StatusCode == Status200OK)
                    {
                        return await RetryPut(uri, body);
                    }
                }
                var ex = e.InnerException as WebException;
                throw;
            }
        }

        public async Task<ApiResponseDto> RetryPost<T>(string uri, T body)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PostAsJsonAsync(uri, body);
                var apiResponse = await response.Content.ReadFromJsonAsync<ApiResponseDto>();
                return apiResponse;
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                return new ApiResponseDto
                {
                    StatusCode = Status500InternalServerError
                };
            }
        }

        public async Task<ApiResponseDto> RetryPut<T>(string uri, T body)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.PutAsJsonAsync(uri, body);
                var apiResponse = await response.Content.ReadFromJsonAsync<ApiResponseDto>();
                return apiResponse;
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
                return new ApiResponseDto
                {
                    StatusCode = Status500InternalServerError
                };
            }
        }

        public async Task<ApiResponseDto> RetryGet(string uri)
        {
            try
            {
                return await _httpClient.GetFromJsonAsync<ApiResponseDto>(uri);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e);
                return new ApiResponseDto
                {
                    StatusCode = Status500InternalServerError
                };
            }
        }
    }
}
