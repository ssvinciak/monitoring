using System.Threading.Tasks;
using Monitoring.Shared.Dto;
using Monitoring.Shared.Dto.Account;

namespace Monitoring.Client.Services.Contracts
{
    public interface IUserProfileApi
    {
        Task<ApiResponseDto> Upsert(UserProfileDto userProfile);

        Task<ApiResponseDto> Get();
    }
}
